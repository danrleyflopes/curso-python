#coding: UTF-8
print('-=' * 10 + '=-' * 10 + '\n|\t\t\t   Desafio 09   \t\t\t|')
print('-=' * 10 + '=-' * 10)
tabuada = int(input('Digite um número para ver sua tabuada: '))
print(u'Soma')
for i in range(1, 10):
    print(str(i), "+ ", tabuada, " = {}".format(tabuada + i))
print(u'Subtração')
for i in range(1, 10):
    print(str(i), "- ", tabuada, " = {}".format(tabuada - i))
print(u'Multiplicação')
for i in range(1, 10):
    print(str(i), "x ", tabuada, " = {}".format(tabuada * i))
print(u'Divisão')
for i in range(1, 10):
    print(str(i), "/ ", tabuada, " = {}".format(tabuada // i))
