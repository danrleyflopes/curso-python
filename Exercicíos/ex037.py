#coding: UTF-8
print('-=' * 10 + '=-' * 10 + '\n|\t\t\t   Desafio 37   \t\t\t|')
print('-=' * 10 + '=-' * 10)
while True:
    num = int(input('Digite um numero inteiro: '))
    print('-=-' * 9 + 'Menu' + '-=-' * 9 + '\n')
    print('- 1 para binario')
    print('- 2 para octal')
    print('- 3 para hexadecimal')
    print('- 4 para decimal')
    comm = str(input('Escolha um dos itens: '))
    qt = len(str(num))
    if comm == '1':
        cvtd = str(bin(num).replace('0b', ''))
        print('{} em dec eh: {}'.format(num, cvtd))
    elif comm == '2':
        cvtd = str(oct(num).replace('0o', ''))
        print('{} em octa eh: {}'.format(num, cvtd))
    elif comm == '3':
        cvtd = str(hex(num).replace('0x', ''))
        print('{} em hexa eh: {}'.format(num, cvtd))
    elif comm == '4':
        byte1 = num // 1 % 2
        byte2 = num // 10 % 2
        byte3 = num // 100 % 2
        byte4 = num // 1000 % 2
        byte5 = num // 10000 % 2
        byte6 = num // 100000 % 2
        byte7 = num // 1000000 % 2
        byte8 = num // 10000000 % 2
        byte9 = num // 100000000 % 2
        byte10 = num// 1000000000 % 2
        byte11 = num// 10000000000 % 2
        byte12 = num// 100000000000 % 2
        byte13 = num// 1000000000000 % 2
        byte14 = num// 10000000000000 % 2
        byte15 = num// 100000000000000 % 2
        byte16 = num// 1000000000000000 % 2
        byte17 = num// 10000000000000000 % 2
        byte18 = num// 100000000000000000 % 2
        byte19 = num// 1000000000000000000 % 2
        byte20 = num// 10000000000000000000 % 2
        byte21 = num// 100000000000000000000 % 2
        byte22 = num// 1000000000000000000000 % 2
        byte23 = num// 10000000000000000000000 % 2
        byte24 = num// 100000000000000000000000 % 2
        byte25 = num// 1000000000000000000000000 % 2
        byte26 = num// 10000000000000000000000000 % 2
        byte27 = num// 100000000000000000000000000 % 2
        byte28 = num// 1000000000000000000000000000 % 2
        byte29 = num// 10000000000000000000000000000 % 2
        byte30 = num// 100000000000000000000000000000 % 2
        byte31 = num// 1000000000000000000000000000000 % 2
        byte32 = num// 10000000000000000000000000000000 % 2
        if byte1 == 1:
            byte1 = 1
        else:
            byte1 = 0
        if byte2 == 1:
            byte2 = 2
        else:
            byte2 = 0
        if byte3 == 1:
            byte3 = 4
        else:
            byte3 = 0
        if byte4 == 1:
            byte4 = 8
        else:
            byte4 = 0
        if byte5 == 1:
            byte5 = 16
        else:
            byte5 = 0
        if byte6 == 1:
            byte6 = 32
        else:
            byte6 = 0
        if byte7 == 1:
            byte7 = 64
        else:
            byte7 = 0
        if byte8 == 1:
            byte8 = 128
        else:
            byte8 = 0
        if byte9 == 1:
            byte9 = 256
        else:
            byte9 = 0
        if byte10 == 1:
            byte10 = 512
        else:
            byte10 = 0
        if byte11 == 1:
            byte11 = 1024
        else:
            byte11 = 0
        if byte12 == 1:
            byte12 = 2048
        else:
            byte12 = 0
        if byte13 == 1:
            byte13 = 4096
        else:
            byte13 = 0
        if byte14 == 1:
            byte14 = 8192
        else:
            byte14 = 0
        if byte15 == 1:
            byte15 = 16384
        else:
            byte15 = 0
        if byte16 == 1:
            byte16 = 32768
        else:
            byte16 = 0
        if byte17 == 1:
            byte17 = 65536
        else:
            byte17 = 0
        if byte18 == 1:
            byte18 = 131072
        else:
            byte18 = 0
        if byte19 == 1:
            byte19 = 262144
        else:
            byte19 = 0
        if byte20 == 1:
            byte20 = 524288
        else:
            byte20 = 0
        if byte21 == 1:
            byte21 = 1048576
        else:
            byte21 = 0
        if byte22 == 1:
            byte22 = 2097152
        else:
            byte22 = 0
        if byte23 == 1:
            byte23 = 4194304
        else:
            byte23 = 0
        if byte24 == 1:
            byte24 = 8388608
        else:
            byte24 = 0
        if byte25 == 1:
            byte25 = 16777216
        else:
            byte25 = 0
        if byte26 == 1:
            byte26 = 33554432
        else:
            byte26 = 0
        if byte27 == 1:
            byte27 = 67108864
        else:
            byte27 = 0
        if byte28 == 1:
            byte28 = 134217728
        else:
            byte28 = 0
        if byte29 == 1:
            byte29 = 268435456
        else:
            byte29 = 0
        if byte30 == 1:
            byte30 = 536870912
        else:
            byte30 = 0
        if byte31 == 1:
            byte31 = 1073741824
        else:
            byte31 = 0
        if byte32 == 1:
            byte32 = 2147483648
        else:
            byte32 = 0
        result = byte1+byte2+byte3+byte4+byte5+byte6+byte7+byte8+byte9+byte10+byte11+byte12+byte13+byte14+byte15+byte16+byte17+byte18+byte19+byte20+byte21+byte22+byte23+byte24+byte25+byte26+byte27+byte28+byte29+byte30+byte31+byte32
        print('{}(b2) em dec eh: {}(b10)'.format(num, result))
    else:
        print('Opcao invalida')
