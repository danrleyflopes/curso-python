#coding: UTF-8
print('-=' * 10 + '=-' * 10 + '\n|\t\t\t   Desafio 43   \t\t\t|')
print('-=' * 10 + '=-' * 10)
peso = float(input('Digite seu peso: '))
altura = float(input('Digite sua altura: '))
imc = peso / altura ** 2
print(imc,'Você está ', end='')
if imc > 0 and imc <= 18.5:
    print('Abaixo do peso.')
elif imc <= 25:
    print('com o Peso ideal.')
elif imc < 30:
    print('com Sobrepeso.')
elif imc <= 40:
    print('com Obesidadde.')
elif imc > 40:
    print('com Obesidade mórbida.')
