#coding: UTF-8
print('-=' * 10 + '=-' * 10 + '\n|\t\t\t   Desafio 14   \t\t\t|')
print('-=' * 10 + '=-' * 10)
c = float(input('Informe a temperatura em graus Celsius: '))
f = 9 * c / 5 + 32
print('A temperatura de {} graus Celsius corresponde a {} graus F'.format(c, f))
#print('A temperatura de {}°C corresponde a {}°F'.format(c, f))
