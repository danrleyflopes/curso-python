#coding: UTF-8
print('-=' * 10 + '=-' * 10 + '\n|\t\t\t   Desafio 05   \t\t\t|')
print('-=' * 10 + '=-' * 10)
n = int(input("Digite um número"))
print("o antecessor do {} é {} e o sucessor do mesmo é {}".format(n, n-1, n+1))
