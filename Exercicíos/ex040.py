n1 = float(input('Qual foi a primeira nota? '))
n2 = float(input('Qual foi a segunda nota? '))
media = (n1 + n2) / 2
if media >= 7:
    print('O aluno está Aprovado.')
elif media >= 5 and media < 7:
    print('O aluno está de recuperação.')
elif media < 5:
    print('O aluno está Reprovado.')