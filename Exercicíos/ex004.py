#coding: UTF-8
print('-=' * 10 + '=-' * 10 + '\n|\t\t\t   Desafio 04   \t\t\t|')
print('-=' * 10 + '=-' * 10)
Tag = input("Digite alguma coisa: ")
print("Tipo da váriavel? " + str(type(Tag)))
#print("Tipo da váriavel? ", type(Tag)) #Outra forma que faz o msm  d linha 2
#print("Tipo da váriavel? {}".format(str(type(Tag))) #Outra forma que faz o msm  d linha 2
#print("Tipo da váriavel?", "{}".format(str(type(Tag))) #Outra forma que faz o msm  d linha 2
print("Somente Caracteres Alphanuméricos? " + str(Tag.isalnum()))
print("Somente Letras? " + str(Tag.isalpha()))
print("Somente números? " + str(Tag.isnumeric()))
print("É Decimal? " + str(Tag.isdecimal()))
print("É Identificável? " + str(Tag.isidentifier()))
print("Somente Digitos? " + str(Tag.isdigit()))
print("Somente Espaços? " + str(Tag.isnumeric()))
print("Tudo em maiúsculo? " + str(Tag.isupper()))
print("Tudo em minúsculo? " + str(Tag.islower()))
print("Está Capitalizado? " + str(Tag.istitle()))
print("É possivel imprimir sem converter? " + str(Tag.isprintable()))
