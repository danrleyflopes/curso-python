#coding: UTF-8
print('-=' * 10 + '=-' * 10 + '\n|\t\t\t   Desafio 21   \t\t\t|')
print('-=' * 10 + '=-' * 10)
import pygame
from time import sleep
import emoji
pygame.mixer.init()
pygame.mixer.music.load(open("seya.mp3","rb"))
pygame.mixer.music.play()
print(emoji.emojize("Tocando... :sunglasses:"))
while pygame.mixer.music.get_busy():
    sleep(1)
print("Pronto!")
