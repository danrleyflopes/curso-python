#coding: UTF-8
print('-=' * 10 + '=-' * 10 + '\n|\t\t\t   Desafio 22   \t\t\t|')
print('-=' * 10 + '=-' * 10)
#Manipulação de Texto
#Fatiamento
#VarString[posIni:PosFim:npulos]
#VarString = 'Eu Sou Um Programador!'
#VarString[10:] começa no 10 até o fim da frase
#VarString[:2] começa no 0 até a posição 2 sem contar a posição 2
#VarString[::2] começa no 0 até o fim pulando de 2 em 2 posições: 'E Su Pormdr'-h
#VarString[:::] = 'Eu Sou Um Programador!'
#Contar 'x' letras
#VarString.Count('letra')
# Contar com fatiamento
#VarString.Count('letra', posIni, PosFim)
#Quantidade de Caracteres
#len(VarString)
#Pesquisar em qual posição se encontra uma cadeia de caracteres dentro de outra
#VarString.find('caractere(s)')
#Remover espaços em branco
#VarString.strip() Esquerda e direita
#VarString.lstrip() Esquerda
#VarString.rstrip() Direita
#Dividir frase em Lista de Palavras
#VarString.split()
#['Eu', 'Sou', 'Um', 'Programador!']
#Antes
#VarString[0] = 'E'
#Depois
#VarString[0] = 'Eu'
#VarString[0][0] = 'E'
#Print multi linha:
#print("""Frase 1
#Frase 2
#Frase 3
#""")
#Comentário Multilinhas:
'''
Sou
um
comentário
multilinhas!
'''
nome = str(input('Digite seu nome completo: '))
print(nome.upper())
print(nome.lower())
print(len(nome) - nome.count(' '))
nome = nome.split()
print(len(nome[0]))
