#coding: UTF-8
print('-=' * 10 + '=-' * 10 + '\n|\t\t\t   Desafio 07   \t\t\t|')
print('-=' * 10 + '=-' * 10)
n1 = float(input("Digite a Nota 1: "))
n2 = float(input("Digite a Nota 2: "))
m = (n1 + n2) / 2
print("Nota Final:", m)
if n1 >= 6:
    print('Aluno Aprovado')
elif n1 <= 5.9:
    print('Aluno Reprovado')
