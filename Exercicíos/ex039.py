#coding: UTF-8
print('-=' * 10 + '=-' * 10 + '\n|\t\t\t   Desafio 39   \t\t\t|')
print('-=' * 10 + '=-' * 10)
from datetime import date
sexo = str(input('Voce eh homem ou mulher? (H/M) '))
if sexo == 'M' or sexo == 'm':
    print('Voce nao precisa fazer o alistamento militar!')
nasc = int(input('Ano de nascimento: '))
idade = date.today().year - nasc
print('Quem nasceu em {} tem {} anos em {}.'.format(nasc, idade, date.today().year))
if idade < 18:
    print('Ainda faltam {} anos para o alistamento'.format(18 - idade))
    print('Seu alistamento sera em {}'.format(nasc + 18))
elif idade == 18:
    print('Voce tem que se alistar imediatamente!')
else: # elif idade > 18:
    print('Voce ja deveria ter se alistado ha {} anos.'.format(idade - 18))
    print('Seu alistamento foi em {}'.format(nasc + 18))