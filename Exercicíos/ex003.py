#coding: UTF-8
print('-=' * 10 + '=-' * 10 + '\n|\t\t\t   Desafio 03   \t\t\t|')
print('-=' * 10 + '=-' * 10)
x = int(input("Digite um número: "))
y = int(input("Digite outro número: "))
print(u"A soma entre {} e {} é igual a {}".format(x, y, x + y))
