#coding: UTF-8
print('-=' * 10 + '=-' * 10 + '\n|\t\t\t   Desafio 42   \t\t\t|')
print('-=' * 10 + '=-' * 10)
r1 = float(input('Digite o comprimento da primeira reta: '))
r2 = float(input('Digite o comprimento da segunda reta: '))
r3 = float(input('Digite o comprimento da terceira reta: '))
if r1 < r2 + r3 and r2 < r1 + r3 and r3 < r1 + r2:
    print('Os segmentos acima podem formar um triângulo', end=' ')
    '''if r1 == r2 and r2 == r3:
        print('Equilátero!')
    elif r1 == r2 and r2 != r3:
        print('Isósceles!')
    elif r2 == r3 and r1 != r3:
        print('Isósceles!')
    elif r1 == r3 and r2 != r3:
        print('Isósceles!')
    elif r1 != r2 and r2 != r3 and r3 != r1:
        print('Escaleno!')'''
    if r1 == r2 == r3:
        print('Equilátero!')
    elif r1 != r2 != r3 != r1:
        print('Escaleno!')
    else:
        print('Isósceles!')
else:
    print('Os segmentos acima não podem formar um triângulo')
