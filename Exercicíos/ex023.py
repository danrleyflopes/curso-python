#coding: UTF-8
print('-=' * 10 + '=-' * 10 + '\n|\t\t\t   Desafio 23   \t\t\t|')
print('-=' * 10 + '=-' * 10)
numero = int(input('Digite um numero entre 0 e 9999: '))
#Sem if():
u = numero // 1 % 10
d = numero // 10 % 10
c = numero // 100 % 10
m = numero // 1000 % 10
print(u, 'Unidade(s)')
print(d, 'Dezena(s)')
print(c, 'Centena(s)')
print(m, 'Milhare(s)')
#Com if():
'''
n = len(numero)
if n == 1:
    print('Unidade: ', numero[0])
elif n == 2:
    print('Dezena: ', numero[0])
    print('Unidade: ',numero[1])
elif n == 3:
    print('Centena: ', numero[0])
    print('Dezena: ', numero[1])
    print('Unidade: ', numero[2])
else:
    print('Milhar: ', numero[0])
    print('Centena: ', numero[1])
    print('Dezena: ', numero[2])
    print('Unidade: ', numero[3])
'''
