#coding: UTF-8
print('-=' * 10 + '=-' * 10 + '\n|\t\t\t   Desafio 29   \t\t\t|')
print('-=' * 10 + '=-' * 10)
SpeedCar = int(input('Qual é a velocidade atual do carro? '))
if SpeedCar > 80:
    print('{}MULTADO! Você excedeu o limite permitido que e de 80Km/h\nVocê deve pagar uma multa de R$ {:.2f}!{}'.format('\033[31m', (SpeedCar - 80) * 7.0,'\033[33m'))
print('{}Tenha um bom dia! Dirija com segurança!'.format('\033[33m'))
