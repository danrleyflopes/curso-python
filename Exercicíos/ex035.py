#coding: UTF-8
print('-=' * 10 + '=-' * 10 + '\n|\t\t\t   Desafio 35   \t\t\t|')
print('-=' * 10 + '=-' * 10)
r1 = float(input('Digite o comprimento da primeira reta: '))
r2 = float(input('Digite o comprimento da segunda reta: '))
r3 = float(input('Digite o comprimento da terceira reta: '))
if r1 < r2 + r3 and r2 < r1 + r3 and r3 < r1 + r2:
    print('Os segmentos acima podem formar um triângulo!')
else:
    print('Os segmentos acima não podem formar um triângulo')
