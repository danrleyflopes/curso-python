#coding: UTF-8
print('-=' * 10 + '=-' * 10 + '\n|\t\t\t   Desafio 15   \t\t\t|')
print('-=' * 10 + '=-' * 10)
qtDias = int(input('Quantos dias alugados? '))
qtKm = float(input('Quantos Km rodados? '))
preço = 60 * qtDias + 0.15 * qtKm
print('O total a pagar é de R$ {}'.format(preço))
