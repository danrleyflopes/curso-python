#coding: UTF-8
print('-=' * 10 + '=-' * 10 + '\n|\t\t\t   Desafio 26   \t\t\t|')
print('-=' * 10 + '=-' * 10)
frase = str(input('Digite uma frase: ')).strip().lower()
print('A letra A aparece {} vez(es) na frase.'.format(frase.count('a')))
print('A primeira letra A aparece na posicao {} da frase.'.format(frase.find('a')))
print('A ultima letra A aparece na posicao {} da frase.'.format(frase.rfind('a')))
