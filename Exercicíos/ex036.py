#coding: UTF-8
print('-=' * 10 + '=-' * 10 + '\n|\t\t\t   Desafio 36   \t\t\t|')
print('-=' * 10 + '=-' * 10)
Casa = float(input('Qual o valor da casa? R$ '))
Salario = float(input('Qual o Salario do comprador? R$ '))
Anos = float(input('Tempo de financiamento? '))
Parcelas = Casa / (Anos * 12)
terco = Salario * 0.30
print('Para pagar uma casa de R$ {:.2f} em {} anos '.format(Casa, Anos), end='')
print('a parcela sera de R$ {:.2f}'.format(Parcelas))
if Parcelas <= terco:
    print('Emprestimo pode ser concedido!')
else:
    print('Emprestimo negado!')