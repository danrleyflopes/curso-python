#coding: UTF-8
print('-=' * 10 + '=-' * 10 + '\n|\t\t\t   Desafio 34   \t\t\t|')
print('-=' * 10 + '=-' * 10)
#Cores
cores = {'limpa':'\033[m',
         'vermelho': '\033[0;31m',
         'verde': '\033[0;32m'}
salário = float(input('Qual é o salário do funcionário? R$'))
aumento = 0
if salário > 1250.00:
    aumento = salário * 0.10 + salário
elif salário <= 1250.00:
    aumento = salário * 0.15 + salário
print('Quem ganhava R${}{:.2f}{} passa a ganhar R${}{:.2f}{} agora.'.format(cores['vermelho'], salário,cores['limpa'], cores['verde'], aumento, cores['limpa']))
if salário >= 5000:
    print('Uau! que salário bom em?')