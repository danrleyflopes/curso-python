#coding: UTF-8
print('-=' * 10 + '=-' * 10 + '\n|\t\t\t   Desafio 28   \t\t\t|')
print('-=' * 10 + '=-' * 10)
from random import randint
while True:
    pensei = randint(1, 5)
    print('-=-' * 30)
    adivinhe = 0
    adivinhe = int(input('Eu Pensei em um número entre 0 e 6 agora resta saber se você adivinha esse número: '))
    if adivinhe > 5 or adivinhe < 1:
        print('Número fora do intervalo')
        continue
    print('-=-' * 30)
    if adivinhe == pensei:
        print('Parabéns você acertou!')
    else:
        print('Haha... Eu venci! pensei no número {}'.format(pensei))
