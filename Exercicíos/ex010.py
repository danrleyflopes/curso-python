#coding: UTF-8
print('-=' * 10 + '=-' * 10 + '\n|\t\t\t   Desafio 10   \t\t\t|')
print('-=' * 10 + '=-' * 10)
Valor = -1
while Valor != '0':
    Valor = input('Digite uma Moeda R$ ou $ um espaco em branco e o valor: ')
    if 'R$' in Valor:
        USD = float(Valor[3:]) * 0.316476
        print(Valor, 'equivale a ${:.2f}'.format(USD))
    elif '$' in Valor:
        BRL = float(Valor[2:]) * 3.15979727
        print(Valor, 'equivale a R$ {:.2f}'.format(BRL))
    else:
        print('Moeda não foi especificada!')
