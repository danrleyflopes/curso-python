#coding: UTF-8
print('-=' * 10 + '=-' * 10 + '\n|\t\t\t   Desafio 11   \t\t\t|')
print('-=' * 10 + '=-' * 10)
largura = float(input('Qual a largura da parede? '))
altura = float(input('Agora eu preciso da altura da parede! '))
área = largura * altura
print('Sua parede tem a dimensão de {}x{} e sua área é de {:.3f}m².'.format(largura, altura, área))
tinta = área / 2
print('Para pintar essa parede, você precisará de {}l de tinta.'.format(tinta))
