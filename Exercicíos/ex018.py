#coding: UTF-8
print('-=' * 10 + '=-' * 10 + '\n|\t\t\t   Desafio 18   \t\t\t|')
print('-=' * 10 + '=-' * 10)
import math
angulo = float(input('Qual o ângulo? '))
seno = math.sin(math.radians(angulo))
cosseno = math.cos(math.radians(angulo))
tangente = math.tan(math.radians(angulo))
print('O ângulo de {}° tem o seno de {:.2f}, seu cosseno é {:.2f} e sua tangente é {:.2f}'.format(angulo, seno, cosseno, tangente))
