#coding: UTF-8
print('-=' * 10 + '=-' * 10 + '\n|\t\t\t   Desafio 41   \t\t\t|')
print('-=' * 10 + '=-' * 10)
from datetime  import date
nasc = date.today().year - int(input('Ano de nascimento: '))
#EuFizAssim:
'''if nasc > 0 and nasc <= 9:
    print('Mirim')
elif nasc > 9 and nasc <= 14:
    print('Infântil')
elif nasc > 14 and nasc <= 19:
    print('Junior')
elif nasc > 19 and nasc <= 25:
    print('Sênior')
elif nasc > 25:
    print('Master')
elif nasc == 0:
    print('Idade nula!')
else:
    print('Você veio do futuro?')'''
#GuanabaraFezAssim:
if nasc < 0:
    print('Você veio do futuro?')
elif nasc == 0:
    print('Idade nula!')
elif nasc > 0 and nasc <= 9:
    print('Mirim')
elif nasc <= 14:
    print('Infântil')
elif nasc <= 19:
    print('Junior')
elif nasc <= 25:
    print('Sênior')
elif nasc > 25:
    print('Master')