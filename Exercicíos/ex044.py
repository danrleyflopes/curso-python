#coding: UTF-8
print('-=' * 10 + '=-' * 10 + '\n|\t\t\t   Desafio 44   \t\t\t|')
print('-=' * 10 + '=-' * 10)
preco = float(input('Digite o preço normal do produto: R$'))
avistadch = preco - (preco * 0.10)
avistacar = preco - (preco * 0.05)
car2x = preco
car3xm = preco + (preco * 0.20)
print('Á vista dinheiro/cheque: R${}'.format(avistadch))
print('Á vista no cartão: R${}'.format(avistacar))
print('Em até 2x no cartão: R${}'.format(car2x))
print('3x ou mais no cartão: R${}'.format(car3xm))