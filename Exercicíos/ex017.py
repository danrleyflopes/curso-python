#coding: UTF-8
print('-=' * 10 + '=-' * 10 + '\n|\t\t\t   Desafio 17   \t\t\t|')
print('-=' * 10 + '=-' * 10)
#import math
#from math import hypot
co = float(input('Comprimento do cateto oposto: '))
ca = float(input('Comprimento do cateto adjacente: '))
'''
#hipo = math.hypot(co ,ca)
#hipo = hypot(co ,ca)
    comentário multilinha usa 3 aspas para começar e terminar!
    comentários com # tbm podem mudar a codificação padrão como acima!
'''
hipo = (co ** 2 + ca ** 2) ** (1/2)
print('A hipotenusa vai medir {:.2f}'.format(hipo))
