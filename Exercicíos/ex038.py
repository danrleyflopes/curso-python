#coding: UTF-8
print('-=' * 10 + '=-' * 10 + '\n|\t\t\t   Desafio 38   \t\t\t|')
print('-=' * 10 + '=-' * 10)
n1 = int(input('Primeiro número: '))
n2 = int(input('Segundo número: '))
if n1 > n2:
    print('O Primeiro valor é maior!')
elif n2 > n1:
    print('O Segundo valor é maior!')
else:
    print('Ambos são iguais!')