#coding: UTF-8
print('-=' * 10 + '=-' * 10 + '\n|\t\t\t   Desafio 20   \t\t\t|')
print('-=' * 10 + '=-' * 10)
from random import shuffle
l1 = str(input('Primeiro aluno: '))
l2 = str(input('Segundo aluno: '))
l3 = str(input('Terceiro aluno: '))
l4 = str(input('Ultimo aluno: '))
lista = [l1, l2, l3, l4]
shuffle(lista)
print('O aluno escolhido foi {}'.format(lista))
