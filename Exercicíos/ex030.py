#coding: UTF-8
print('-=' * 10 + '=-' * 10 + '\n|\t\t\t   Desafio 30   \t\t\t|')
print('-=' * 10 + '=-' * 10)
num = int(input('Me diga um número qualquer: '))
if num % 2 == 0:
    print('{} é Par!'.format('\033[4;32m' + str(num) + '\033[;;;m'))
else:
    print('{} é Impar!'.format('\033[4;31m' + str(num) + '\033[;;;m'))
