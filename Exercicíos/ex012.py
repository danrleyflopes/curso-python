#coding: UTF-8
print('-=' * 10 + '=-' * 10 + '\n|\t\t\t   Desafio 12   \t\t\t|')
print('-=' * 10 + '=-' * 10)
preco = float(input("Qual é o preço do produto? R$ "))
desconto = preco - (preco * 0.05)
print("O produto que custava R$ {:.2f}, na promoção com desconto de 5 por cento vai custar R$ {:.2f}".format(preco, desconto))
