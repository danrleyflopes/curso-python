#coding: UTF-8
print('-=' * 10 + '=-' * 10 + '\n|\t\t\t   Desafio 16   \t\t\t|')
print('-=' * 10 + '=-' * 10)
#import math
#from math import trunc
pontoFlutuante = float(input('Digite um número fracionário: '))
#print('O valor digitado foi {} e a sua porção inteira é {}'.format(pontoFlutuante, int(pontoFlutuante)))
print('O valor digitado foi {} e a sua porção inteira é {:.0f}'.format(pontoFlutuante, pontoFlutuante))
#print('O valor digitado foi {} e a sua porção inteira é {:.0f}'.format(pontoFlutuante, math.trunc(pontoFlutuante)))
#print('O valor digitado foi {} e a sua porção inteira é {:.0f}'.format(pontoFlutuante, trunc(pontoFlutuante)))
