#coding: UTF-8
print('-=' * 10 + '=-' * 10 + '\n|\t\t\t   Desafio 13   \t\t\t|')
print('-=' * 10 + '=-' * 10)
salário = float(input('Qual o salário do Funcionário? R$ '))
aumento = salário + (salário * 0.15)
print('Um funcionário que recebia R$ {:.2f}, com 15% de aumento, passa a receber R$ {:.2f}'.format(salário, aumento))
