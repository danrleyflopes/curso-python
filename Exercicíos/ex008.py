#coding: UTF-8
print('-=' * 10 + '=-' * 10 + '\n|\t\t\t   Desafio 08   \t\t\t|')
print('-=' * 10 + '=-' * 10)
m = float(input('Digite uma distancia em metros: '))
km = m / 1000
hm = m / 100
dam = m / 10
#m = m
dm = m * 10
cm = m * 100
mm = m * 1000
print('{} Km'.format(km))
print('{} Hm'.format(hm))
print('{} Dam'.format(dam))
print('{} m'.format(m))
print('{} dm'.format(dm))
print('{} cm'.format(cm))
print('{} mm'.format(mm))
